FROM ruby:2.6.10-bullseye
ENV TZ="Europe/London"
RUN gem install bundler:2.2.6
ENV RAILS_ENV=test
ENV HOME=/home/app
ENV DISABLE_SPRING=true
ENV DEBIAN_FRONTEND=noninteractive
RUN addgroup app --gid 1000
RUN adduser --uid 1000 --gid 1000 --shell /bin/bash --home /home/app app
RUN mkdir /builds && touch /builds/.keep && chmod -R 0744 /builds && chown -R app:app /builds
COPY --chown=1000:1000 ./builds/.keep /builds/
RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null && \
        echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list && \
        curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get update && apt-get install -y -qq gpg bash tzdata gettext sudo shared-mime-info mariadb-client libmariadb-dev libidn11-dev nodejs yarn git build-essential curl libsqlite3-dev nodejs yarn
RUN wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
RUN sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update
RUN apt-get install -y google-chrome-stable
RUN apt-get update && apt-get -y install google-chrome-stable
USER app
ENV HOME /home/app
WORKDIR /home/app/
CMD ["/bin/bash"]
